require "#{Rails.root}/app/models/web_spellchecker.rb"

class DictionaryWordsController < ApplicationController
  def spellcheck
	input_word = params[:term]

	webchecker = WebSpellchecker.new
	webcheck = Hash.new

	webcheck = {"term" => input_word, "known" => webchecker.correct(input_word), "suggestions" => webchecker.correct(input_word)}

    render json: webcheck
  end
end
